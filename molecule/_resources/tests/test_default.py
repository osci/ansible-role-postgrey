import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_service(host):
    srv = host.service("postgrey")
    assert srv.is_running
    assert srv.is_enabled

def test_socket(host):
    host.socket("unix:///var/spool/postfix/postgrey/socket").is_listening

